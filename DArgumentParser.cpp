#include "DArgumentParser.h"

#include <algorithm>

constexpr std::string_view helpSectionUsageString = "Usage: ";
constexpr std::string_view helpSectionPosArgString = "\nArguments:\n";
constexpr std::string_view argOptionTakesValueString = "<value> ";
constexpr std::string_view helpAndVersionOptionsSectionOpeningString = "\nGetting help:\n";
constexpr std::string_view normalOptionSectionOpeningString = "\nOptions:\n";
constexpr char minusSign = '-', equalSign = '=';
constexpr int shortCommandStartPos = 1, longCommandStartPos = 2;

/* ------ DArgumentOption ------ */
DArgumentOption::DArgumentOption() = default;

DArgumentOption::DArgumentOption(std::set<char> &&_shortCommands, std::set<std::string> &&_longCommands, std::string _description) : shortCommands(_shortCommands), longCommands(_longCommands), description(std::move(_description)) {}

DArgumentOption::DArgumentOption(const DArgumentOptionType _type, std::set<char> &&_shortCommands, std::set<std::string> &&_longCommands, std::string _description) : type(_type), shortCommands(_shortCommands), longCommands(_longCommands), description(std::move(_description)) {}

DArgumentOption::DArgumentOption(const DArgumentOptionType _type, std::string _description) : type(_type), description(std::move(_description)) {}

bool DArgumentOption::AddShortCommand(const char shortCommand) {
    if (shortCommand < 33 || shortCommand == minusSign || shortCommand == 127)
        return false;
    return shortCommands.insert(shortCommand).second;
}

bool DArgumentOption::AddShortCommand(std::set<char> &&_shortCommands) {
    for (const auto shortCommand: _shortCommands)
        if (shortCommand < 33 || shortCommand == minusSign || shortCommand == 127)
            return false;
    shortCommands.merge(_shortCommands);
    return true;
}

const std::set<char> &DArgumentOption::ShortCommands() const {
    return shortCommands;
}

void DArgumentOption::ClearShortCommands() {
    shortCommands.clear();
}

bool DArgumentOption::AddLongCommand(const std::string &longCommand) {
    if (longCommand.front() == minusSign || longCommand.find(equalSign) != std::string::npos)
        return false;
    return longCommands.insert(longCommand).second;
}

bool DArgumentOption::AddLongCommand(std::set<std::string> &&_longCommands) {
    for (auto longCommand: _longCommands)
        if (longCommand.front() == minusSign || longCommand.find(equalSign) != std::string::npos)
            return false;
    longCommands.merge(_longCommands);
    return true;
}

const std::set<std::string> &DArgumentOption::LongCommands() const {
    return longCommands;
}

void DArgumentOption::ClearLongCommands() {
    longCommands.clear();
}

void DArgumentOption::AddDescription(const std::string &_description) {
    description = _description;
}

DArgumentOptionType DArgumentOption::GetType() const {
    return type;
}

void DArgumentOption::SetType(const DArgumentOptionType _type) {
    type = _type;
}

int DArgumentOption::WasSet() const {
    return wasSet;
}

const std::string &DArgumentOption::GetValue() const {
    return value;
}

DArgumentParser::DArgumentParser(const int argc, char **argv, std::string _appName, std::string _appVersion, std::string _appDescription) : argumentCount(argc), argumentValues(argv), executableName(getExecutableName(argv[0])), appName(std::move(_appName)), appVersion(std::move(_appVersion)), appDescription(std::move(_appDescription)) {}

std::string DArgumentParser::getExecutableName(const char *execCall) {
    const std::string execName(execCall);
    return execName.substr(execName.find_last_of('/') + 1);
}

std::string DArgumentParser::generateUsageSection() {
    const std::string_view optionString = argumentOptions.empty() ? "" : " [options]";
    std::string posArgString;
    if (!positionalArgs.empty()) {
        std::string::size_type totalSize = 0;
        for (const auto &posArg: positionalArgs)
            totalSize += std::get<2>(posArg).empty() ? std::get<0>(posArg).size() + 3 : std::get<2>(posArg).size() + 1;
        posArgString.reserve(totalSize);
        for (const auto &posArg: positionalArgs) {
            if (std::get<2>(posArg).empty()) {
                posArgString += " [";
                posArgString += std::get<0>(posArg);
                posArgString += ']';
                continue;
            }
            posArgString += ' ';
            posArgString += std::get<2>(posArg);
        }
    }
    std::string usageString;
    usageString.reserve(helpSectionUsageString.size() + executableName.size() + optionString.size() + posArgString.size() + 1);
    usageString += helpSectionUsageString;
    usageString += executableName;
    usageString += optionString;
    usageString += posArgString;
    usageString += '\n';
    return usageString;
}

std::string DArgumentParser::generateDescriptionSection() const {
    if (appDescription.empty())
        return {};
    std::string descriptionString;
    descriptionString.reserve(2 + appDescription.size());
    descriptionString += '\n';
    descriptionString += appDescription;
    descriptionString += '\n';
    return descriptionString;
}

std::pair<size_t, size_t> DArgumentParser::calculateSizeOfPosArgColumnAndString() const {
    size_t colSize = 0, totalSize = 0;
    for (const auto &arg: positionalArgs) {
        if (const size_t currColSize = std::get<2>(arg).empty() ? std::get<0>(arg).size() + 2 : std::get<2>(arg).size(); currColSize > colSize)
            colSize = currColSize;
        totalSize += std::get<1>(arg).size();
    }
    totalSize += 7 * positionalArgs.size() + colSize * positionalArgs.size();
    return {colSize, totalSize};
}

std::string DArgumentParser::generatePositionalArgsSection() {
    if (positionalArgs.empty())
        return {};
    std::string argSection;
    const auto [optionArgsColSize, totalSize] = calculateSizeOfPosArgColumnAndString();
    argSection.reserve(totalSize + helpSectionPosArgString.size());
    argSection += helpSectionPosArgString;
    for (const auto &arg: positionalArgs) {
        argSection.append(3, ' ');
        const size_t currIndex = argSection.size();
        if (!std::get<2>(arg).empty())
            argSection += std::get<2>(arg);
        else {
            argSection += '[';
            argSection += std::get<0>(arg);
            argSection += ']';
        }
        if (const auto diff = optionArgsColSize - (argSection.size() - currIndex); diff < optionArgsColSize)
            argSection.append(diff, ' ');
        argSection.append(3, ' ');
        argSection += std::get<1>(arg);
        argSection += '\n';
    }
    return argSection;
}

size_t DArgumentParser::calculateSizeOfOptionColumn(const std::vector<std::reference_wrapper<DArgumentOption>> &args) {
    size_t size = 0;
    for (const auto &arg: args) {
        /** formula explanation:
         * 2 * arg->shortCommands.size() -> adding the minus sign to a characters always leads to 2 characters, so we just need to multiply the amount of chars by 2
         * arg->shortCommands.size() + arg->longCommands.size() -> the amount of spaces needed between commands, not reducing by 1 means we'll have one extra space total, but we can account for that when building the string and avoid branches.
         * (arg->type == DArgumentOptionType::InputOption) * 8) -> if arg.type takes a value, we'll print "<value> " (8 characters) after it, because false/true resolves to 0/1, we can safely multiply by this comparison to avoid branching.
         */
        size_t currSize = 2 * arg.get().shortCommands.size() + arg.get().shortCommands.size() + arg.get().longCommands.size() + (arg.get().type == DArgumentOptionType::InputOption) * argOptionTakesValueString.size();
        auto iterator = arg.get().longCommands.begin(), end = arg.get().longCommands.end();
        while (iterator != end) {
            currSize += 2 + iterator->size(); //size of string plus two minus signs characters
            ++iterator;
        }
        if (currSize > size)
            size = currSize;
    }
    return size;
}

std::pair<std::vector<std::string>, size_t> DArgumentParser::generateOptionStrings(const std::vector<std::reference_wrapper<DArgumentOption>> &args, const size_t columnSize) {
    std::vector<std::string> optionStrings;
    optionStrings.reserve(args.size());
    size_t totalSize = 0;
    for (const auto &arg: args) {
        std::string tempStr;
        tempStr.reserve(6 + columnSize + arg.get().description.size());
        tempStr.append(3, ' ');
        for (const auto c: arg.get().shortCommands) {
            tempStr += minusSign;
            tempStr += c;
            tempStr += ' ';
        }
        for (const auto &str: arg.get().longCommands) {
            tempStr += "--";
            tempStr += str;
            tempStr += ' ';
        }
        if (arg.get().type == DArgumentOptionType::InputOption)
            tempStr += argOptionTakesValueString;
        if (const size_t leftoverChars = 3 + columnSize - tempStr.size(); leftoverChars > 0)
            tempStr.append(leftoverChars, ' ');
        if (!arg.get().description.empty()) {
            tempStr.append(2, ' ');
            tempStr += arg.get().description;
        }
        tempStr += '\n';
        totalSize += tempStr.size();
        optionStrings.emplace_back(std::move(tempStr));
    }
    return {optionStrings, totalSize};
}

std::string DArgumentParser::generateOptionsSubSection(const std::vector<std::reference_wrapper<DArgumentOption>> &args, const std::string_view &openingString, bool shouldSort) {
    std::string sectionString;
    const size_t optionCommandsColSize = calculateSizeOfOptionColumn(args);
    auto [orderedOptionStrings, totalSize] = generateOptionStrings(args, optionCommandsColSize);
    if (shouldSort)
        std::sort(orderedOptionStrings.begin(), orderedOptionStrings.end());
    sectionString.reserve(totalSize + openingString.size());
    sectionString += openingString;
    for (const auto &str: orderedOptionStrings)
        sectionString += str;
    return sectionString;
}

std::string DArgumentParser::generateOptionsSection() const {
    std::vector<std::reference_wrapper<DArgumentOption>> helpAndVersionOptions, normalOptions;
    for (auto &arg: argumentOptions)
        if (arg.get().type == DArgumentOptionType::HelpOption || arg.get().type == DArgumentOptionType::VersionOption)
            helpAndVersionOptions.push_back(arg);
        else
            normalOptions.push_back(arg);
    std::string sectionString;
    if (!helpAndVersionOptions.empty())
        sectionString += generateOptionsSubSection(helpAndVersionOptions, helpAndVersionOptionsSectionOpeningString, settings.sortOptions);
    if (!normalOptions.empty())
        sectionString += generateOptionsSubSection(normalOptions, normalOptionSectionOpeningString, settings.sortOptions);
    return sectionString;
}

void DArgumentParser::generateErrorText(const DParseResult error, std::string command) {
    if (!command.empty())
        command = (command.size() > 1 ? "--" : "-") + command;
    switch (error) {
        case DParseResult::InvalidOption:
            errorText = "Option " + command + " is invalid";
            break;
        case DParseResult::ValuePassedToOptionThatDoesNotTakeValue:
            errorText = "Option " + command + " received a value but it doesn't take any";
            break;
        case DParseResult::NoValueWasPassedToOption:
            errorText = "Option " + command + " takes a value but none was passed.";
            break;
        case DParseResult::OptionsThatTakesValueNeedsToBeSetSeparately:
            errorText = "Options that takes a value needs to be set separately. Error with option: " + command;
            break;
        case DParseResult::OptionsFoundAfterPositionalArgument:
            errorText = "Option found after first positional argument.";
            break;
        case DParseResult::ParseSuccessful:
        default:
            return;
    }
}

bool DArgumentParser::isLongCommand(const std::string &argument) {
    return argument.size() > longCommandStartPos && argument[0] == minusSign && argument[1] == minusSign;
}

bool DArgumentParser::isValidLongCommand(const std::string &argument) const {
    if (argument.size() > longCommandStartPos && argument[0] == minusSign && argument[1] == minusSign) {
        const size_t posOfEqualSign = argument.find_first_of(equalSign, longCommandStartPos);
        const std::string command = argument.substr(longCommandStartPos, posOfEqualSign - longCommandStartPos);
        for (const auto &arg: argumentOptions) {
            if (arg.get().longCommands.find(command) == arg.get().longCommands.end())
                continue;
            return true;
        }
    }
    return false;
}

bool DArgumentParser::isShortCommand(const std::string &argument) {
    return argument.size() > shortCommandStartPos && argument[0] == minusSign && argument[1] != minusSign;
}

bool DArgumentParser::isValidShortCommand(const std::string &argument) {
    if (argument.size() > shortCommandStartPos && argument[0] == minusSign && argument[1] != minusSign) {
        std::string command = argument.substr(shortCommandStartPos);
        return std::all_of(command.begin(), command.end(), [this](const char c) {
            return std::any_of(argumentOptions.begin(), argumentOptions.end(), [c](const DArgumentOption &arg) {
                if (arg.shortCommands.find(c) == arg.shortCommands.end())
                    return false;
                return true;
            });
        });
    }
    return false;
}

bool DArgumentParser::checkIfArgumentIsUnique(DArgumentOption &dArgumentOption) {
    if (argumentOptions.find(std::ref(dArgumentOption)) != argumentOptions.end())
        return false;
    if (dArgumentOption.shortCommands.empty() && dArgumentOption.longCommands.empty())
        return false;
    for (const auto &argument: argumentOptions) {
        auto shortEnd = argument.get().shortCommands.end();
        for (auto shortCommand: dArgumentOption.shortCommands)
            if (argument.get().shortCommands.find(shortCommand) != shortEnd)
                return false;
        auto longEnd = argument.get().longCommands.end();
        for (auto &longCommand: dArgumentOption.longCommands)
            if (argument.get().longCommands.find(longCommand) != longEnd)
                return false;
    }
    return true;
}

bool DArgumentParser::checkIfAllArgumentsInListAreUnique(const std::unordered_set<std::reference_wrapper<DArgumentOption>> &_arguments) {
    for (auto upperIterator = _arguments.begin(), upperEnd = _arguments.end(); upperIterator != upperEnd; ++upperIterator) {
        if (!checkIfArgumentIsUnique(*upperIterator))
            return false;
        auto lowerIterator = upperIterator;
        if (++lowerIterator == upperEnd)
            break;
        for (; lowerIterator != upperEnd; ++lowerIterator) {
            auto shortEnd = lowerIterator->get().shortCommands.end();
            for (auto shortCommand: upperIterator->get().shortCommands)
                if (lowerIterator->get().shortCommands.find(shortCommand) != shortEnd)
                    return false;
            auto longEnd = lowerIterator->get().longCommands.end();
            for (auto &longCommand: upperIterator->get().longCommands)
                if (lowerIterator->get().longCommands.find(longCommand) != longEnd)
                    return false;
        }
    }
    return true;
}

void DArgumentParser::resetParsedValues() {
    positionalArgsValues.clear();
    errorText.clear();
    for (const auto &arg: argumentOptions) {
        arg.get().wasSet = 0;
        arg.get().value.clear();
    }
}

DParseResult DArgumentParser::parseLongCommand(const std::string &argument, int &currentIndex) {
    const size_t posOfEqualSign = argument.find_first_of(equalSign, longCommandStartPos);
    const std::string command = argument.substr(longCommandStartPos, posOfEqualSign - longCommandStartPos);
    bool commandFound = false;
    for (const auto &arg: argumentOptions) {
        if (arg.get().longCommands.find(command) == arg.get().longCommands.end())
            continue;
        commandFound = true;
        if (arg.get().type != DArgumentOptionType::InputOption && posOfEqualSign != std::string::npos) {
            generateErrorText(DParseResult::ValuePassedToOptionThatDoesNotTakeValue, command);
            return DParseResult::ValuePassedToOptionThatDoesNotTakeValue;
        }
        if (arg.get().type == DArgumentOptionType::InputOption) {
            if (posOfEqualSign == std::string::npos) {
                if (++currentIndex == argumentCount) {
                    generateErrorText(DParseResult::NoValueWasPassedToOption, command);
                    return DParseResult::NoValueWasPassedToOption;
                }
                arg.get().value = std::string(argumentValues[currentIndex]);
                if (!settings.inputOptionAcceptsOtherOptionsAsInput && (isValidLongCommand(arg.get().value) || isValidShortCommand(arg.get().value))) {
                    generateErrorText(DParseResult::NoValueWasPassedToOption, command);
                    return DParseResult::NoValueWasPassedToOption;
                }
            } else {
                if (posOfEqualSign == argument.size() - 1) {
                    generateErrorText(DParseResult::NoValueWasPassedToOption, command);
                    return DParseResult::NoValueWasPassedToOption;
                }
                arg.get().value = argument.substr(posOfEqualSign + 1);
            }
        }
        arg.get().wasSet++;
        break;
    }
    if (!commandFound) {
        generateErrorText(DParseResult::InvalidOption, command);
        return DParseResult::InvalidOption;
    }
    return DParseResult::ParseSuccessful;
}

DParseResult DArgumentParser::parseShortCommand(const std::string &argument, int &currentIndex) {
    for (int i = 1; i < argument.size(); i++) {
        bool commandFound = false;
        for (const auto &arg: argumentOptions) {
            if (arg.get().shortCommands.find(argument[i]) == arg.get().shortCommands.end())
                continue;
            commandFound = true;
            if (arg.get().type == DArgumentOptionType::InputOption && argument.size() > 2) {
                generateErrorText(DParseResult::OptionsThatTakesValueNeedsToBeSetSeparately, std::string(1, argument[i]));
                return DParseResult::OptionsThatTakesValueNeedsToBeSetSeparately;
            }
            if (arg.get().type == DArgumentOptionType::InputOption) {
                if (++currentIndex == argumentCount) {
                    generateErrorText(DParseResult::NoValueWasPassedToOption, std::string(1, argument[i]));
                    return DParseResult::NoValueWasPassedToOption;
                }
                arg.get().value = std::string(argumentValues[currentIndex]);
                if (!settings.inputOptionAcceptsOtherOptionsAsInput && (isValidLongCommand(arg.get().value) || isValidShortCommand(arg.get().value))) {
                    generateErrorText(DParseResult::NoValueWasPassedToOption, std::string(1, argument[i]));
                    return DParseResult::NoValueWasPassedToOption;
                }
            }
            arg.get().wasSet++;
            break;
        }
        if (!commandFound) {
            generateErrorText(DParseResult::InvalidOption, std::string(1, argument[i]));
            return DParseResult::InvalidOption;
        }
    }
    return DParseResult::ParseSuccessful;
}

void DArgumentParser::SetAppInfo(const std::string &name, const std::string &version, const std::string &description) {
    appName = name;
    appVersion = version;
    appDescription = description;
}

void DArgumentParser::SetAppName(const std::string &name) {
    appName = name;
}

void DArgumentParser::SetAppVersion(const std::string &version) {
    appVersion = version;
}

void DArgumentParser::SetAppDescription(const std::string &description) {
    appDescription = description;
}

void DArgumentParser::EnableHelpTextOptionsSorting() {
    settings.sortOptions = true;
}

void DArgumentParser::DisableHelpTextOptionsSorting() {
    settings.sortOptions = false;
}

void DArgumentParser::EnableInputOptionAcceptsOtherOptionsAsInput() {
    settings.inputOptionAcceptsOtherOptionsAsInput = true;
}

void DArgumentParser::DisableInputOptionAcceptsOtherOptionsAsInput() {
    settings.inputOptionAcceptsOtherOptionsAsInput = false;
}

void DArgumentParser::EnableStrictOptionPosition() {
    settings.strictOptionPosition = true;
}

void DArgumentParser::DisableStrictOptionPosition() {
    settings.strictOptionPosition = false;
}

void DArgumentParser::EnableOptionsAfterFirstPosArgAsPosArg() {
    settings.optionsAfterFirstPosArgAsPosArg = true;
}

void DArgumentParser::DisableOptionsAfterFirstPosArgAsPosArg() {
    settings.optionsAfterFirstPosArgAsPosArg = false;
}

bool DArgumentParser::AddArgumentOption(DArgumentOption &dArgumentOption) {
    if (!checkIfArgumentIsUnique(dArgumentOption))
        return false;
    return argumentOptions.insert(std::ref(dArgumentOption)).second;
}

bool DArgumentParser::AddArgumentOption(std::unordered_set<std::reference_wrapper<DArgumentOption>> &&args) {
    if (!checkIfAllArgumentsInListAreUnique(args))
        return false;
    argumentOptions.merge(args);
    return true;
}

bool DArgumentParser::RemoveArgumentOption(DArgumentOption &argument) {
    return argumentOptions.erase(std::ref(argument));
}

void DArgumentParser::ClearArgumentOptions() {
    argumentOptions.clear();
}

void DArgumentParser::AddPositionalArgument(std::string name, std::string description, std::string syntax) {
    positionalArgs.emplace_back(std::move(name), std::move(description), std::move(syntax));
}

void DArgumentParser::ClearPositionalArgumets() {
    positionalArgs.clear();
}

int DArgumentParser::WasSet(const char command) const {
    for (const auto &argument: argumentOptions)
        if (argument.get().shortCommands.find(command) != argument.get().shortCommands.end())
            return argument.get().wasSet;
    return 0;
}

int DArgumentParser::WasSet(const std::string &command) const {
    for (const auto &argument: argumentOptions)
        if (argument.get().longCommands.find(command) != argument.get().longCommands.end())
            return argument.get().wasSet;
    return 0;
}

const std::vector<std::string> &DArgumentParser::GetPositionalArguments() const {
    return positionalArgsValues;
}

std::string DArgumentParser::VersionText() const {
    std::string versionText;
    versionText.reserve(appName.size() + appVersion.size() + 2);
    versionText.append(appName);
    versionText += ' ';
    versionText.append(appVersion);
    versionText += '\n';
    return versionText;
}

std::string DArgumentParser::HelpText() {
    std::string helpText;
    const std::string usageSection = generateUsageSection();
    const std::string descriptionSection = generateDescriptionSection();
    const std::string posArgsSection = generatePositionalArgsSection();
    const std::string optionsSection = generateOptionsSection();
    helpText.reserve(usageSection.size() + descriptionSection.size() + posArgsSection.size() + optionsSection.size());
    helpText += usageSection;
    helpText += descriptionSection;
    helpText += posArgsSection;
    helpText += optionsSection;
    return helpText;
}

std::string DArgumentParser::ErrorText() const {
    return errorText;
}

DParseResult DArgumentParser::Parse() {
    resetParsedValues();
    if (argumentCount < 2)
        return DParseResult::ParseSuccessful;
    DParseResult parseResult;
    bool foundFirstPosArgument = false;
    for (int index = 1; index < argumentCount; index++) {
        std::string currArg(argumentValues[index]);
        if (!foundFirstPosArgument || settings.strictOptionPosition) {
            if (isLongCommand(currArg)) {
                if (foundFirstPosArgument) {
                    generateErrorText(DParseResult::OptionsFoundAfterPositionalArgument);
                    return DParseResult::OptionsFoundAfterPositionalArgument;
                }
                parseResult = parseLongCommand(currArg, index);
                if (parseResult != DParseResult::ParseSuccessful)
                    return parseResult;
                continue;
            }
            if (isShortCommand(currArg)) {
                if (foundFirstPosArgument) {
                    generateErrorText(DParseResult::OptionsFoundAfterPositionalArgument);
                    return DParseResult::OptionsFoundAfterPositionalArgument;
                }
                parseResult = parseShortCommand(currArg, index);
                if (parseResult != DParseResult::ParseSuccessful)
                    return parseResult;
                continue;
            }
        }
        positionalArgsValues.push_back(currArg);
        if (!foundFirstPosArgument && (settings.optionsAfterFirstPosArgAsPosArg || settings.strictOptionPosition))
            foundFirstPosArgument = true;
    }
    return DParseResult::ParseSuccessful;
}
