#ifndef DARGUMENTPARSER_LIBRARY_H
#define DARGUMENTPARSER_LIBRARY_H

#include <string>
#include <vector>
#include <set>
#include <unordered_set>

enum class DParseResult : unsigned char {
    ParseSuccessful,
    InvalidOption,
    ValuePassedToOptionThatDoesNotTakeValue,
    NoValueWasPassedToOption,
    OptionsThatTakesValueNeedsToBeSetSeparately,
    OptionsFoundAfterPositionalArgument,
};

enum class DArgumentOptionType : unsigned char {
    NormalOption,
    InputOption,
    HelpOption,
    VersionOption,
};

class DArgumentOption {
    friend class DArgumentParser;

    DArgumentOptionType type = DArgumentOptionType::NormalOption;
    int wasSet = 0;
    std::string value;
    std::set<char> shortCommands;
    std::set<std::string> longCommands;
    std::string description;

public:
    /**
     * @details instantiate a DArgumentOption with default values (type = DArgumentOptionType::NormalOption)
     */
    explicit DArgumentOption();

    DArgumentOption(std::set<char> &&_shortCommands, std::set<std::string> &&_longCommands, std::string _description = std::string());

    DArgumentOption(DArgumentOptionType _type, std::set<char> &&_shortCommands, std::set<std::string> &&_longCommands, std::string _description = std::string());

    DArgumentOption(DArgumentOptionType _type, std::string _description);

    /**
     * Adds the passed character to the command list for this option, unless it was already included.
     * @return true if command was added, false if command was invalid¹ or not added.
     * @note ¹invalid - any character without a ascii representation, spaces or the character minus(-).
     */
    bool AddShortCommand(char shortCommand);

    /**
     * Adds the passed characters to the command list for this option, if any of the characters were already added it won't be added again.
     * @return false if any of the commands were invalid¹, otherwise true.
     * @note ¹invalid - any character without a ascii representation, spaces or the character minus(-).
     */
    bool AddShortCommand(std::set<char> &&_shortCommands);

    [[nodiscard]] const std::set<char> &ShortCommands() const;

    void ClearShortCommands();

    /**
     * Adds the passed string to the command list for this option, unless it was already included.
     * @return true if command was added, false if command was invalid¹ or not added.
     * @note ¹invalid - if string starts with a minus(-) sign or has a equal(=) sign.
     */
    bool AddLongCommand(const std::string &longCommand);

    /**
     * Adds the passed strings to the command list for this option, if any of the strings were already added it won't be added again.
     * @return false if any of the commands were invalid¹, otherwise true.
     * @note ¹invalid - if string starts with a minus(-) sign or has a equal(=) sign.
     */
    bool AddLongCommand(std::set<std::string> &&_longCommands);

    [[nodiscard]] const std::set<std::string> &LongCommands() const;

    void ClearLongCommands();

    void AddDescription(const std::string &_description);

    void SetType(DArgumentOptionType _type);

    [[nodiscard]] DArgumentOptionType GetType() const;

    [[nodiscard]] int WasSet() const;

    [[nodiscard]] const std::string &GetValue() const;
};

template<>
struct std::hash<std::reference_wrapper<DArgumentOption>> {
    size_t operator()(const std::reference_wrapper<DArgumentOption> &x) const noexcept {
        return std::hash<DArgumentOption *>()(&x.get());
    }
};

template<>
struct std::equal_to<std::reference_wrapper<DArgumentOption>> {
    bool operator()(const std::reference_wrapper<DArgumentOption> &left, const std::reference_wrapper<DArgumentOption> &right) const {
        return &left.get() == &right.get();
    }
};

class DArgumentParser {
    struct ParserSettings {
        bool sortOptions = true;
        bool inputOptionAcceptsOtherOptionsAsInput = false;
        bool strictOptionPosition = false;
        bool optionsAfterFirstPosArgAsPosArg = false;
    } settings;
    int argumentCount;
    char **argumentValues;
    const std::string executableName;
    std::string appName;
    std::string appVersion;
    std::string appDescription;
    std::unordered_set<std::reference_wrapper<DArgumentOption>> argumentOptions;
    std::vector<std::tuple<std::string, std::string, std::string>> positionalArgs;
    std::vector<std::string> positionalArgsValues;
    std::string errorText;

    static std::string getExecutableName(const char *execCall);

    std::string generateUsageSection();

    std::string generateDescriptionSection() const;

    std::pair<size_t, size_t> calculateSizeOfPosArgColumnAndString() const;

    std::string generatePositionalArgsSection();

    static size_t calculateSizeOfOptionColumn(const std::vector<std::reference_wrapper<DArgumentOption>> &args);

    static std::pair<std::vector<std::string>, size_t> generateOptionStrings(const std::vector<std::reference_wrapper<DArgumentOption>> &args, size_t columnSize);

    static std::string generateOptionsSubSection(const std::vector<std::reference_wrapper<DArgumentOption>> &args, const std::string_view &openingString, bool shouldSort);

    std::string generateOptionsSection() const;

    void generateErrorText(DParseResult error, std::string command = std::string());

    static bool isLongCommand(const std::string &argument);

    bool isValidLongCommand (const std::string &argument) const;

    static bool isShortCommand(const std::string &argument);

    bool isValidShortCommand (const std::string &argument);

    bool checkIfArgumentIsUnique(DArgumentOption &dArgumentOption);

    bool checkIfAllArgumentsInListAreUnique(const std::unordered_set<std::reference_wrapper<DArgumentOption>> &_arguments);

    void resetParsedValues();

    DParseResult parseLongCommand(const std::string &argument, int &currentIndex);

    DParseResult parseShortCommand(const std::string &argument, int &currentIndex);

public:
    DArgumentParser(int argc, char **argv, std::string _appName = std::string(), std::string _appVersion = std::string(), std::string _appDescription = std::string());

    void SetAppInfo(const std::string &name, const std::string &version, const std::string &description = std::string());

    void SetAppName(const std::string &name);

    void SetAppVersion(const std::string &version);

    void SetAppDescription(const std::string &description);

    /**
     * Enable options sorting when generating the HelpText string.
     * @note Enabled by default
     */
    void EnableHelpTextOptionsSorting();

    /**
     * Disable options sorting when generating the HelpText string.
     * @note Enabled by default
     */
    void DisableHelpTextOptionsSorting();

    /**
     * When enabled options will not check if their input (the next argument in line) is a valid option.
     * @details Does not apply when input is passed through an equal sign: `--option=value`
     * @note Disabled by default
     */
    void EnableInputOptionAcceptsOtherOptionsAsInput();

    /**
     * When enabled options will not check if their input (the next argument in line) is a valid option.
     * @details Does not apply when input is passed through an equal sign: `--option=value`
     * @note Disabled by default
     */
    void DisableInputOptionAcceptsOtherOptionsAsInput();

    /**
     * When enabled options will only be accepted if they come before positional arguments
     * @note Disabled by default
     */
    void EnableStrictOptionPosition();

    /**
     * When enabled options will only be accepted if they come before positional arguments
     * @note Disabled by default
     */
    void DisableStrictOptionPosition();

    /**
     * When enabled all arguments, including options, after the first positional argument will be taken as positional arguments
     * @details When `StrictOptionPosition` is enabled, this setting has no effect.
     * @note Disabled by default
     */
    void EnableOptionsAfterFirstPosArgAsPosArg();

    /**
     * When enabled all arguments, including options, after the first positional argument will be taken as positional arguments
     * @details When `StrictOptionPosition` is enabled, this setting has no effect.
     * @note Disabled by default
     */
    void DisableOptionsAfterFirstPosArgAsPosArg();

    /**
     * <br>if the argument is valid¹ then it will be added to the argument list.
     * @return true if argument was added, false if it wasn't (invalid argument).
     * @note ¹valid - At least 1 command, either long or short, is set and all of its commands are unique (when compared to other DArgumentOptions added before).
     */
    bool AddArgumentOption(DArgumentOption &dArgumentOption);

    /**
     * <br>if all argumentOptions are valid¹ then they will be added to the argument list.
     * @return true if the argumentOptions were added, false if they were not. (at least one argument was invalid).
     * @note ¹valid - At least 1 command, either long or short, is set for each argument and all commands are unique (when compared to other DArgumentOptions added before and to each other).
     */
    bool AddArgumentOption(std::unordered_set<std::reference_wrapper<DArgumentOption>> &&args);

    /**
     * Removes the passed argument from the argument list.
     * @return true if it was removed, false if it wasn't (in case there was no such argument in the list).
     */
    bool RemoveArgumentOption(DArgumentOption &argument);

    /**
     * Removes all argumentOptions previously added, clearing the list.
     */
    void ClearArgumentOptions();

    /**
     * <br>Used to generate the help string.
     * @param name Name of the command.
     * @param description The description to be shown.
     * @param syntax How the command should be used, useful for more complex applications. (Optional, will default to the passed name if not set).
     */
    void AddPositionalArgument(std::string name, std::string description, std::string syntax = std::string());

    /**
     * Removes all positional arguments previously added, clearing the list.
     */
    void ClearPositionalArgumets();

    /**
     * @param command the command character to check.
     * @return Returns a boolean indicating if the option was set or not, always returns false if no option with specified command was found.
     */
    [[nodiscard]] int WasSet(char command) const;

    /**
     * @param command the command string to check.
     * @return Returns a boolean indicating if the option was set or not, always returns false if no option with specified command was found.
     */
    [[nodiscard]] int WasSet(const std::string &command) const;

    /**
     * Retrieves the value of every positional argument that was set during the parsing.
     * @return Returns a const reference to the positionalArgsValues list.
     */
    [[nodiscard]] const std::vector<std::string> &GetPositionalArguments() const;

    [[nodiscard]] std::string VersionText() const;

    [[nodiscard]] std::string HelpText();

    [[nodiscard]] std::string ErrorText() const;

    /**
     * <br>Parses the argv passed on creation based on the positional arguments and option arguments added.
     * @return true if parse was successful, false if an error occurred (non-optional parameter not passed). Call "ErrorText" function to retrieve a printable string of the error.
     */
    DParseResult Parse();
};

#endif //DARGUMENTPARSER_LIBRARY_H
